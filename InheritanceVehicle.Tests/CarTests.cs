﻿using System.Reflection;
using NUnit.Framework;

namespace InheritanceVehicle.Tests
{
    [TestFixture]
    public class CarTests
    {
        private const string CarClassName = "car";
        private const string VehicleClassName = "vehicle";
        private Type carType;

        [SetUp]
        public void Initialize()
        {
            var assembly = typeof(Stub).Assembly;
            this.carType = assembly.GetTypes().FirstOrDefault(t => t.Name.Equals(CarClassName, StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void Car_Class_Is_Created()
        {
            Assert.IsNotNull(this.carType, "'Car' class is not created.");
        }

        [Test]
        public void Car_Inherits_Vehicle()
        {
            var carInstance = Activator.CreateInstance(this.carType, string.Empty, 0);
            var vehicleType = typeof(Stub).Assembly.GetTypes().FirstOrDefault(t => t.Name.Equals(VehicleClassName, StringComparison.OrdinalIgnoreCase));

            Assert.IsInstanceOf(vehicleType, carInstance, "'Car' type does NOT inherit 'Vehicle' type.");
        }

        [Test]
        public void Set_Name_Method_Is_Defined()
        {
            var method = this.carType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    var parameters = m.GetParameters();
                    if (m.ReturnType == typeof(void) && parameters?.FirstOrDefault()?.ParameterType == typeof(string))
                    {
                        return true;
                    }

                    return false;
                });

            Assert.IsNotNull(method, "Method which changes 'Car' name is NOT define or it does NOT contain correct parameters.");
        }

        [Test]
        public void Get_Name_Method_Is_Defined()
        {
            var method = this.carType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    if (m.ReturnType == typeof(string) && m.GetParameters().Length == 0)
                    {
                        return true;
                    }

                    return false;
                });

            Assert.IsNotNull(method, "Method which retrieves 'Car' name is NOT define or it is's return type is NOT correct.");
        }

        [Test]
        public void Get_Car_Name()
        {
            var name = "Toyota";
            var newName = "BMW";
            var age = 5;
            var carInstance = Activator.CreateInstance(this.carType, name, age);

            var setNameMethod = this.carType
                .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    var parameters = m.GetParameters();
                    if (m.ReturnType == typeof(void) && parameters?.FirstOrDefault()?.ParameterType == typeof(string))
                    {
                        return true;
                    }

                    return false;
                });

            var getNameMethod = this.carType
                .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .FirstOrDefault(m =>
                {
                    if (m.ReturnType == typeof(string) && m.GetParameters().Length == 0)
                    {
                        return true;
                    }

                    return false;
                });

            setNameMethod.Invoke(carInstance, new[] { newName });

            var carName = getNameMethod.Invoke(carInstance, Array.Empty<object>());

            Assert.AreEqual(
                newName,
                carName,
                $"'{getNameMethod.Name}' method does NOT return correct value or '{setNameMethod.Name}' method does NOT change car name correctly.");
        }

        [Test]
        public void Is_Start()
        {
            bool expected = true;
            Car car = new Car("BMW", 200);

            car.StartCar();
            bool actual = car.IsCarStart;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Is_Stop()
        {
            bool expected = false;
            Car car = new Car("BMW", 200);

            car.StopCar();
            bool actual = car.IsCarStart;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Acceleration_Test()
        {
            int expected = 25;
            Car car = new Car("BMW", 200);

            car.StartCar();
            car.AccelerationCar(15);
            car.AccelerationCar(20);
            car.AccelerationCar(-10);
            int actual = car.Speed;

            Assert.AreEqual(expected, actual);
        }

        [TestCase("BMW", 200, 15, "left", "turn straight and left")]
        [TestCase("Volvo", 120, 25, "right", "turn straight and right")]
        [TestCase("Mercedes", 200, -15, "left", "turn back and left")]
        [TestCase("Bugatti", 400, -25, "right", "turn back and right")]
        [TestCase("Maybach", 300, 0, "right", null)]
        public void Drive_Test(string name, int maxSpeed, int addSpeed, string movement, string expected)
        {
            Car car = new Car(name, maxSpeed);

            car.StartCar();
            car.AccelerationCar(addSpeed);
            car.DriveCar(movement);
            string actual = car.Movement;

            Assert.AreEqual(expected, actual);
        }

        [TestCase("BMW", 200, 15, null)]
        [TestCase("Volvo", 120, 25, "rright")]
        [TestCase("Mercedes", 200, 1, "leftf")]
        public void Drive_Test_With_Wrong(string name, int maxSpeed, int addSpeed, string movement)
        {
            Car car = new Car(name, maxSpeed);
            car.StartCar();
            car.AccelerationCar(addSpeed);

            Assert.Throws<ArgumentException>(() => car.DriveCar(movement));
        }
    }
}
