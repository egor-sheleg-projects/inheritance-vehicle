﻿using System.Runtime.CompilerServices;

namespace InheritanceVehicle
{
    public class Car : Vehicle
    {
        private readonly int maxSpeed = 100;
        private readonly int minSpeed = -30;

        public Car(string name, int maxSpeed)
            : base(name, maxSpeed)
        {
        }

        public bool IsCarStart { get; set; }

        public string Movement { get; set; }

        public int Speed { get; set; }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return this.Name;
        }

        public virtual void StartCar()
        {
            this.IsCarStart = true;
        }

        public virtual void StopCar()
        {
            this.Speed = 0;
            this.IsCarStart = false;
        }

        public virtual void DriveCar(string direction)
        {
            if (this.IsCarStart && this.Speed != 0)
            {
                if (this.Speed > 0)
                {
                    this.Movement = "straight";
                }
                else if (this.Speed < 0)
                {
                    this.Movement = "back";
                }

                if (direction == "right" || direction == "left")
                {
                    this.Movement = "turn " + this.Movement + " and " + direction;
                }
                else
                {
                    throw new ArgumentException("Direction argument is valid");
                }
            }
            else
            {
                this.StopCar();
            }
        }

        public virtual void AccelerationCar(int acceleration)
        {
            if (this.IsCarStart)
            {
                if (this.Speed + acceleration == 0)
                {
                    this.StopCar();
                }
                else if (this.Speed + acceleration >= this.maxSpeed)
                {
                    this.Speed = 100;
                }
                else if (this.Speed + acceleration <= this.minSpeed)
                {
                    this.Speed = -30;
                }
                else
                {
                    this.Speed += acceleration;
                }
            }
        }
    }
}
