﻿namespace InheritanceVehicle
{
    public class Vehicle
    {
        private readonly int maxSpeed;

        public Vehicle(string name, int maxSpeed)
        {
            this.Name = name;
            this.maxSpeed = maxSpeed;
        }

        public int MaxSpeed
        {
            get { return this.maxSpeed; }
        }

        private protected string Name { get; set; }
    }
}
